package main;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Rule;
import net.sourceforge.jFuzzyLogic.rule.Variable;

public class MainFuzzy {
	
	public static void main(String[] args) {
		// Load from 'FCL' file
		String fileName = "fcl/hospital.fcl";
		FIS fis = FIS.load(fileName, true);
		// Error while loading?
		if (fis == null) {
			System.err.println("Can't load file: '" + fileName + "'");
			return;
		}

		// Show
		JFuzzyChart.get().chart(fis.getFunctionBlock("job_chooser"));

		// Set inputs
		fis.setVariable("temperatura", 1);
		fis.setVariable("kihanje", 1);
		fis.setVariable("kasljanje", 1);
		fis.setVariable("grlobolja", 1);
		fis.setVariable("dusenje", 1);
		fis.setVariable("glavobolja", 1);
		fis.setVariable("pluca", 1);

		// Evaluate
		fis.evaluate();
		
		// Show output variable's chart
		Variable tip = fis.getVariable("decision");
		JFuzzyChart.get().chart(tip, tip.getDefuzzifier(), true);

		// Print ruleSet
		System.out.println(fis);

		System.out.println("REZULTAT:");

		System.out.println(fis.getVariable("decision").getValue());

		for (Rule r : fis.getFunctionBlock("job_chooser")
				.getFuzzyRuleBlock("job_rules").getRules())
			System.out.println(r);

	}

}
